(package-initialize)

(load-theme 'misterioso t)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(setq package-archive-enable-alist '(("melpa" deft magit)))

(add-to-list 'load-path "~/.emacs.d/elpa")

(setq-default cursor-type 'bar)

(setq make-backup-files nil)

(setq auto-save-default nil)

(setq-default tab-width 2)

(setq-default indent-tabs-mode nil)

(setq inhibit-startup-message t)

(fset 'yes-or-no-p 'y-or-n-p)

(setq-default frame-title-format '("%f"))

(set-face-attribute 'default nil :height 100)

;;how emacs looks
(menu-bar-mode -1)

(toggle-scroll-bar -1)

(tool-bar-mode -1)

(delete-selection-mode t)

(scroll-bar-mode -1)

(blink-cursor-mode t)

(show-paren-mode t)

(column-number-mode t)

(global-linum-mode 1)  ;show line-numbers

(setq linum-format "%4d ")

(set-fringe-style -1)

(tooltip-mode -1)

;;utf8

(setq locale-coding-system 'utf-8)

(set-terminal-coding-system 'utf-8)

(set-keyboard-coding-system 'utf-8)

(set-selection-coding-system 'utf-8)

(prefer-coding-system 'utf-8)

;;add and delete trailing lines

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq delete-trailing-lines t)

(setq require-final-newline t)    ;add a new line at the end of file

(setq next-line-add-newlines nil) ;add a new line when at the end of buffer

(define-key global-map (kbd "RET") 'newline-and-indent)

;;cut copy paste undo

(setq x-select-enable-clipboard t)

(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)

(global-set-key "\C-z" 'undo)

(global-set-key "\M-z" 'redo)

(define-key global-map [(control a)] 'mark-whole-buffer)

(require 'yasnippet)
(require 'haml-mode)
(require 'scss-mode)

(add-hook 'yaml-mode-hook
        (lambda ()
            (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

(yas-global-mode 1)

(add-to-list 'load-path "/home/santanu/dev/neotree")
  (require 'neotree)
  (global-set-key [f8] 'neotree-toggle)

(defun toggle-selective-display (column)

      (interactive "P")

      (set-selective-display

       (or column

           (unless selective-display

             (1+ (current-column))))))

(defun toggle-hiding (column)

      (interactive "P")

      (if hs-minor-mode

          (if (condition-case nil

                  (hs-toggle-hiding)

                (error t))

              (hs-show-all))

        (toggle-selective-display column)))

(load-library "hideshow")

    (global-set-key (kbd "C-+") 'toggle-hiding)

(cua-mode t)

    (setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands

    (transient-mark-mode 1) ;; No region when it is not highlighted

    (setq cua-keep-region-after-copy t) ;; Standard Windows behaviour

;;windmove

;;better tan C-x o

;;(when (fboundp 'windmove-default-keybindings)

;;  (windmove-default-keybindings))

(global-set-key (kbd "C-c a")  'windmove-left)

(global-set-key (kbd "C-c d") 'windmove-right)

(global-set-key (kbd "C-c w")    'windmove-up)

(global-set-key (kbd "C-c s")  'windmove-down)

;;(setq shift-select-mode t)

;;copy-paste previous line

(fset 'pprev

   [?\C-p ?\C-a ?\C-  ?\C-n ?\M-w ?\C- ?\C-y])

;;customized eshell-prompt

;;(load "~/.emacs.d/eshell/eshell_customize.el")

(require 'auto-dictionary)

(add-hook 'flyspell-mode-hook (lambda () (auto-dictionary-mode 1)))

(require 'autopair)

(autopair-global-mode) ;; to enable in all buffers

(require 'auto-complete)

(require 'auto-complete-config)

  (global-auto-complete-mode t)

(require 'web-mode)

  (set-face-attribute 'web-mode-html-tag-face nil :foreground "#dd0000")

  (set-face-attribute 'web-mode-html-attr-name-face nil :foreground "Pink5")

(add-to-list 'auto-mode-alist '("\\.jsx\\'" . jsx-mode))

(add-hook 'jsx-mode-hook

          (lambda () (auto-complete-mode 1)))

(add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))

(setq web-mode-markup-indent-offset 2)

(setq web-mode-code-indent-offset 2)

(setq web-mode-enable-auto-pairing t)

(require 're-builder)

(setq reb-re-syntax 'string)

;;ruby rails setup

(add-hook 'ruby-mode-hook 'robe-mode)

(require 'ruby-end)

(add-hook 'ruby-mode-hook 'projectile-mode)

(setq projectile-enable-caching t)

(add-hook 'ruby-mode-hook 'projectile-rails-mode)

;;(add-to-list 'auto-mode-alist '("\\.jsx\\'" . jsx-mode))

;;(autoload 'jsx-mode "jsx-mode" "JSX mode" t)

;;2 space indentation for css and js

(setq css-indent-offset 2)

(setq js-indent-level 2)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(custom-enabled-themes (quote (tango-dark)))
 '(custom-safe-themes
   (quote
    ("4f11aa6dbd2f270962f41bc5721027838154254aa8f45003845c3495b32ddf50" "a041a61c0387c57bb65150f002862ebcfe41135a3e3425268de24200b82d6ec9" default)))
 '(fci-rule-color "#383838")
 '(js2-basic-offset 2)
 '(js2-bounce-indent-p t)
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("melpa" . "http://melpa.milkbox.net/packages/"))))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(send-mail-function (quote mailclient-send-it))
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3"))

(setq jsx-indent-level 2)

;;emmet snippets

(require 'emmet-mode)

(add-hook 'html-mode-hook 'emmet-mode)

(add-hook 'css-mode-hook  'emmet-mode)

(fset 'g

   [?\M-x ?e ?m ?m ?e ?t ?- ?e ?x ?p ?a ?n ?d ?- ?l ?i ?n ?e return ?\C-x])

(fset 'ey

   [?\M-x ?e ?m ?m ?e ?t ?- ?e ?x ?p ?a ?n ?d ?- ?y ?a ?s return ?\C-x])

(fset 'y

      [?\M-x ?y ?a ?s ?- ?e ?x ?p ?a ?n ?d return ?\C-x])

;;toggle window split

(defun toggle-window-split ()

  (interactive)

  (if (= (count-windows) 2)

      (let* ((this-win-buffer (window-buffer))

         (next-win-buffer (window-buffer (next-window)))

         (this-win-edges (window-edges (selected-window)))

         (next-win-edges (window-edges (next-window)))

         (this-win-2nd (not (and (<= (car this-win-edges)

                     (car next-win-edges))

                     (<= (cadr this-win-edges)

                     (cadr next-win-edges)))))

         (splitter

          (if (= (car this-win-edges)

             (car (window-edges (next-window))))

          'split-window-horizontally

        'split-window-vertically)))

    (delete-other-windows)

    (let ((first-win (selected-window)))

      (funcall splitter)

      (if this-win-2nd (other-window 1))

      (set-window-buffer (selected-window) this-win-buffer)

      (set-window-buffer (next-window) next-win-buffer)

      (select-window first-win)

      (if this-win-2nd (other-window 1))))))

(global-set-key (kbd "C-x 5") 'toggle-window-split)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)