# emacs settings
# How to use it.

1. Place this code [code-snippet](https://bitbucket.org/santanu_bhattacharya/emacs-settings/src/14eca36ba6a023354ba2acfafe67c3683677fdd7/.emacs?at=master) at ~/.emacs file
2. Install required packages.
3. press Alt + x in emacs
4. type package-install
5. then press enter
6. type package name(like yasnippet)
7. repeat same for other packages to install
8. restart emacs
9. if packages are not available, press Alt + x in emacs, type list-packages and install the packages, restart emacs.
10. Troubleshoot: if .emacs file is not loading properly, debug emacs opening. Write in the console , emacs --debug-init, and it will show the error, like if some packages are missing and we ve to install it.

